from django.urls import path
from todos.views import (todo_list,
                         task_list,
                         create_list_form,
                         update_list_form,
                         delete_list_form,
                         create_new_task,
                         update_task,
                         )


urlpatterns = [
    path('', todo_list, name='todo_list_list'),
    path('<int:id>/', task_list, name='todo_list_detail'),
    path('create/', create_list_form, name='todo_list_create'),
    path('<int:id>/edit/', update_list_form, name='todo_list_update'),
    path('<int:id>/delete/', delete_list_form, name='todo_list_delete'),
    path('items/create/', create_new_task, name='todo_item_create'),
    path('items/<int:id>/edit/', update_task, name='todo_item_update'),
]
