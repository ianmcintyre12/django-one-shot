from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TaskForm
# Create your views here.


def todo_list(request):
    todolist = TodoList.objects.all()
    context = {
        "todo_list": todolist
        }

    return render(request, "todo_list/list.html", context)


def task_list(request, id):
    task_list = get_object_or_404(TodoList, id=id)
    context = {
        'tasks': task_list
    }

    return render(request, 'todo_list/detail.html', context)


def create_list_form(request):
    if request.method == 'POST':
        form = TodoListForm(request.POST)
        if form.is_valid():
            TodoList = form.save()
            return redirect('todo_list_detail', id=TodoList.id)
    else:
        form = TodoListForm()

    context = {
        'forms': form
    }

    return render(request, 'todo_list/create.html', context)


def update_list_form(request, id):
    todo_list = TodoList.objects.get(id=id)
    if request.method == 'POST':
        form = TodoListForm(request.POST,  instance=todo_list)
        if form.is_valid():
            todo_list = form.save()
            return redirect('todo_list_detail', id=todo_list.id)
    else:
        form = TodoListForm(instance=todo_list)

    context = {
        'forms': form
    }

    return render(request, 'todo_list/edit.html', context)


def delete_list_form(request, id):
    todo_list = TodoList.objects.get(id=id)
    if request.method == 'POST':
        todo_list.delete()
        return redirect('todo_list_list')

    return render(request, 'todo_list/delete.html.')


def create_new_task(request):
    if request.method == 'POST':
        form = TaskForm(request.POST)
        if form.is_valid():
            todo_task = form.save()
            return redirect('todo_list_detail', id=todo_task.list.id)
    else:
        form = TaskForm()

    context = {
        'forms': form
    }

    return render(request, 'todo_list/create_item.html', context)


def update_task(request, id):
    task = TodoItem.objects.get(id=id)
    if request.method == 'POST':
        form = TaskForm(request.POST, instance=task)
        if form.is_valid():
            task = form.save()
            return redirect('todo_list_detail', id=task.list.id)
    else:
        form = TaskForm(instance=task)

    context = {
        'forms': form
    }

    return render(request, 'todo_list/edit_item.html', context)
